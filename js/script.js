/* ------------ Header ------------ */
const navigation = document.querySelector(".navigation");
const navbar_button = document.querySelector(".navbar-button");
const close_navbar_button = document.querySelector(".close-navbar-button");
const account_sub_menus = document.querySelectorAll(".account-sub-menu");

navbar_button.addEventListener("click", () => {
  navigation.style.display = "block";
  close_navbar_button.style.display = "block";
  const arrows = document.querySelectorAll(".fa-angle-right");
  arrows.forEach((arrow) => {
    arrow.classList.remove("fa-angle-right");
    arrow.classList.add("fa-angle-down");
  });
});
close_navbar_button.addEventListener("click", () => {
  navigation.style.display = "none";
  const arrows_up = document.querySelectorAll(".fa-angle-up");
  const arrows_down = document.querySelectorAll(".fa-angle-down");
  arrows_up.forEach((arrow) => {
    arrow.classList.remove("fa-angle-up");
    arrow.classList.add("fa-angle-right");
  });
  arrows_down.forEach((arrow) => {
    arrow.classList.remove("fa-angle-down");
    arrow.classList.add("fa-angle-right");
  });
  account_sub_menus.forEach((menu) => {
    menu.classList.remove("show");
  });
});

const listAccountOptions = document.querySelectorAll(
  ".account-list-options>li"
);
listAccountOptions.forEach((option) => {
  option.addEventListener("click", () => {
    const sub_menu = option.getElementsByClassName("account-sub-menu")[0];
    const arrow = option.querySelector("span>i");
    if (sub_menu.classList.contains("show")) {
      arrow.classList.remove("fa-angle-up");
      arrow.classList.add("fa-angle-down");
      sub_menu.classList.remove("show");
    } else {
      sub_menu.classList.add("show");
      arrow.classList.remove("fa-angle-down");
      arrow.classList.add("fa-angle-up");
      listAccountOptions.forEach((another_option) => {
        if (option != another_option) {
          const another_sub_menu =
            another_option.getElementsByClassName("account-sub-menu")[0];
          const another_arrow = another_option.querySelector("span>i");
          another_arrow.classList.remove("fa-angle-up");
          another_arrow.classList.add("fa-angle-down");
          another_sub_menu.classList.remove("show");
        }
      });
    }
  });
});

const about_form_fields = document.querySelectorAll(".about-form-field");
about_form_fields.forEach((field) => {
  const input = field.querySelector("input");
  input.addEventListener("focus", () => {
    const label = field.querySelector("label");
    label.style.fontSize = "11px";
    label.style.top = "-0.5rem";
    field.style.borderColor = "blue";
  });
  input.addEventListener("blur", () => {
    if (!input.value) {
      const label = field.querySelector("label");
      label.style.fontSize = "1.0625rem";
      label.style.top = "0";
      field.style.borderColor = "#f1f4f8";
    }
  });
});

/* ------------ Download a sample ------------ */
const nameInput = document.getElementById("about-form-name");
const emailInput = document.getElementById("about-form-email");
const passwordInput = document.getElementById("about-form-password");
const nameValidationNotifi = document.querySelector(".name-validation-wrapper");
const emailValidationNotifi = document.querySelector(
  ".email-validation-wrapper"
);
const passwordValidationNotifi = document.querySelector(
  ".password-validation-wrapper"
);
const download_button = document.querySelector(".about-button");
download_button.addEventListener("click", () => {
  validationProcess();
});
nameInput.addEventListener("focus", () => {
  nameInput.addEventListener("keypress", (event) => {
    if (event.key == "Enter") {
      validationProcess();
    }
  });
});
emailInput.addEventListener("focus", () => {
  emailInput.addEventListener("keypress", (event) => {
    if (event.key == "Enter") {
      validationProcess();
    }
  });
});
passwordInput.addEventListener("focus", () => {
  passwordInput.addEventListener("keypress", (event) => {
    if (event.key == "Enter") {
      validationProcess();
    }
  });
});
function validationProcess() {
  const namevalue = nameInput.value.trim();
  const emailvalue = emailInput.value.trim();
  const passwordvalue = passwordInput.value.trim();
  const checkedName = checkNameValue(namevalue);
  const checkedEmail = checkEmailValue(emailvalue);
  const checkedPassword = checkPasswordValue(passwordvalue);
  processPopUp(checkedName, checkedEmail, checkedPassword);
}

function processPopUp(nameReturn, emailReturn, passwordReturn) {
  if (!nameReturn) {
    nameValidationNotifi.classList.add("show");
    setTimeout(() => {
      nameValidationNotifi.classList.remove("show");
    }, 2000);
  }
  if (!emailReturn) {
    emailValidationNotifi.classList.add("show");
    setTimeout(() => {
      emailValidationNotifi.classList.remove("show");
    }, 2000);
  }
  if (!passwordReturn) {
    passwordValidationNotifi.classList.add("show");
    setTimeout(() => {
      passwordValidationNotifi.classList.remove("show");
    }, 2000);
  }
}
function checkNameValue(name) {
  if (name.length == 0) return false;
  else return true;
}
function checkEmailValue(email) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) return true;
  else false;
}
function checkPasswordValue(password) {
  if (password.length < 8) return false;
  else return true;
}

/* ------------ Word running ------------ */
const textRunning = document.querySelector(".text-running");
const textCursor = document.querySelector(".text-cursor");
const allTextRunning = ["developers.", "founders.", "designers."];
var textNumber = 0;
var textIndex = 0;
var intervalVar;
function writingText() {
  var text = allTextRunning[textNumber].slice(0, textIndex + 1);
  textRunning.innerHTML = text;
  textIndex++;
  if (text === allTextRunning[textNumber]) {
    clearInterval(intervalVar);

    var cursorInterval = setInterval(() => {
      if (textCursor.style.display === "none")
        textCursor.style.display = "inline-block";
      else textCursor.style.display = "none";
    }, 250);

    setTimeout(() => {
      clearInterval(cursorInterval);
      textCursor.style.display = "inline-block";
      intervalVar = setInterval(deletingText, 50);
    }, 1000);
  }
}

function deletingText() {
  var text = allTextRunning[textNumber].slice(0, textIndex - 1);
  textRunning.innerHTML = text;
  textIndex--;

  if (text === "") {
    clearInterval(intervalVar);
    if (textNumber == allTextRunning.length - 1) textNumber = 0;
    else textNumber++;
    textIndex = 0;

    setTimeout(() => {
      intervalVar = setInterval(writingText, 50);
    }, 200);
  }
}

intervalVar = setInterval(writingText, 50);

/* ------------ Slider-img ------------ */
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides((slideIndex += n));
}
const nextbutton = document.querySelector(".btn-next");
const previousbutton = document.querySelector(".btn-prev");
nextbutton.addEventListener("click", () => {
  plusSlides(1);
  plusTextSlider(1);
});
nextbutton.addEventListener("mousedown", () => {
  nextbutton.style.opacity = "0.5";
});
nextbutton.addEventListener("mouseup", () => {
  nextbutton.style.opacity = "1";
});
previousbutton.addEventListener("click", () => {
  plusSlides(-1);
  plusTextSlider(-1);
});
previousbutton.addEventListener("mousedown", () => {
  previousbutton.style.opacity = "0.5";
});
previousbutton.addEventListener("mouseup", () => {
  previousbutton.style.opacity = "1";
});
function showSlides(n) {
  var i;
  const slides = document.querySelectorAll(".testimonials-img-wrapper");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
}

/* ------------ Slider-text ------------ */
var textSliderIndex = 1;
const sliderClassList = [
  "left-hidden-slider",
  "right-hidden-slider",
  "left-show-slider",
  "right-show-slider",
];

function plusTextSlider(n) {
  if (n > 0) {
    showTextSlider(1);
  } else {
    showTextSlider(-1);
  }
}

function showTextSlider(flag) {
  var i;
  const textSlides = document.querySelectorAll(".testimonials-card-wrapper");
  for (i = 0; i < textSlides.length; i++) {
    sliderClassList.forEach((class_value) => {
      if (textSlides[i].classList.contains(class_value)) {
        textSlides[i].classList.remove(class_value);
      }
    });
  }
  if (flag > 0) {
    var nextTextSlider = textSliderIndex + 1;
    if (nextTextSlider > textSlides.length) nextTextSlider = 1;
    textSlides[nextTextSlider - 1].style.transform = "translate(-100%,-50%)";
    textSlides[textSliderIndex - 1].classList.add("right-hidden-slider");
    textSlides[nextTextSlider - 1].classList.add("right-show-slider");
    setTimeout(() => {
      textSlides[textSliderIndex - 1].style.transform = "translate(100%,-50%)";
      textSlides[nextTextSlider - 1].style.transform = "translate(0%,-50%)";
      textSliderIndex = nextTextSlider;
    }, 500);
  } else {
    var nextTextSlider = textSliderIndex - 1;
    if (nextTextSlider < 1) nextTextSlider = textSlides.length;
    textSlides[nextTextSlider - 1].style.transform = "translate(100%,-50%)";
    textSlides[textSliderIndex - 1].classList.add("left-hidden-slider");
    textSlides[nextTextSlider - 1].classList.add("left-show-slider");
    setTimeout(() => {
      textSlides[textSliderIndex - 1].style.transform = "translate(100%,-50%)";
      textSlides[nextTextSlider - 1].style.transform = "translate(0%,-50%)";
      textSliderIndex = nextTextSlider;
    }, 500);
  }
}

const testimonialsCard = document.querySelector(".testimonials-card-wrapper");
const testimonialsLowerRight = document.querySelector(
  ".testimonials-lower-right"
);
window.addEventListener("resize", () => {
  if (window.innerWidth < 760) {
    testimonialsLowerRight.style.height = testimonialsCard.offsetHeight + "px";
  }
});

/* ------------ Pricing ------------ */
const checkboxInput = document.querySelector(".pricing-input");
const pricingAmount = document.querySelector(".pricing-amount");
const annualPrice = 29;
const monthlyPrice = 49;
var currentPrice = 29;
var priceInterval;
checkboxInput.addEventListener("change", () => {
  if (checkboxInput.checked) {
    priceInterval = setInterval(numberIncreasing, 30);
  } else {
    priceInterval = setInterval(numberDecreasing, 30);
  }
});

function numberIncreasing() {
  currentPrice++;
  if (currentPrice == monthlyPrice) {
    clearInterval(priceInterval);
    setTimeout(() => {
      var tempText = currentPrice.toString();
      pricingAmount.innerHTML = tempText;
    }, 150);
    return;
  }
  var text = currentPrice.toString();
  pricingAmount.innerHTML = text;
}

function numberDecreasing() {
  currentPrice--;
  if (currentPrice == annualPrice) {
    clearInterval(priceInterval);
    setTimeout(() => {
      var tempText = currentPrice.toString();
      pricingAmount.innerHTML = tempText;
    }, 150);
    return;
  }
  var text = currentPrice.toString();
  pricingAmount.innerHTML = text;
}

/* ------------ Sections ------------ */
const satisfaction = document.querySelector(".satisfaction");
const hoursSupport = document.querySelector(".hours-support");
const daysSupport = document.querySelector(".days-support");
const sales = document.querySelector(".sales");
const statsData = document.querySelector(".stats-data");

function numberIncreasingRecursion(start, stop, interactElement, delay) {
  if (start <= stop) {
    interactElement.innerHTML = start;
    start++;
    if (start > stop - 3) {
      setTimeout(() => {
        numberIncreasingRecursion(start, stop, interactElement);
      }, 150);
    } else {
      setTimeout(() => {
        numberIncreasingRecursion(start, stop, interactElement);
      }, delay);
    }
  }
}

const welcomeColumns = document.querySelectorAll(".welcome-column");
const featuresColumns = document.querySelectorAll(".features-column");
const aboutLefts = document.querySelectorAll(".about-left-column");
const aboutRights = document.querySelectorAll(".about-right-column");
const pricingColumns = document.querySelectorAll(".pricing-bot-column");

function isInViewport(element) {
  const rect = element.getBoundingClientRect();
  return (
    rect.top <= (window.innerHeight || document.documentElement.clientHeight)
  );
}

window.addEventListener("load", () => {
  welcomeColumns.forEach((welcomeColumn) => {
    if (isInViewport(welcomeColumn)) {
      welcomeColumn.classList.add("go-up");
    }
  });
  featuresColumns.forEach((featuresColumn) => {
    if (isInViewport(featuresColumn)) {
      featuresColumn.classList.add("go-up");
    }
  });
  aboutLefts.forEach((aboutLeft) => {
    if (isInViewport(aboutLeft)) {
      aboutLeft.classList.add("go-right");
    }
  });
  aboutRights.forEach((aboutRight) => {
    if (isInViewport(aboutRight)) {
      aboutRight.classList.add("go-left");
    }
  });
  if (
    isInViewport(statsData) &&
    !statsData.classList.contains("increased-number")
  ) {
    statsData.classList.add("increased-number");
    numberIncreasingRecursion(0, 100, satisfaction, 10);
    numberIncreasingRecursion(0, 24, hoursSupport, 30);
    numberIncreasingRecursion(0, 7, daysSupport, 30);
    numberIncreasingRecursion(0, 100, sales, 10);
  }
  if (isInViewport(pricingColumns[0])) {
    pricingColumns[0].classList.add("go-up");
  }
  if (isInViewport(pricingColumns[1])) {
    pricingColumns[1].style.opacity = "0";
    setTimeout(() => {
      pricingColumns[1].style.opacity = "1";
      pricingColumns[1].classList.add("go-up");
    }, 250);
  }
});

window.addEventListener("scroll", () => {
  welcomeColumns.forEach((welcomeColumn) => {
    if (isInViewport(welcomeColumn)) {
      welcomeColumn.classList.add("go-up");
    }
  });
  featuresColumns.forEach((featuresColumn) => {
    if (isInViewport(featuresColumn)) {
      featuresColumn.classList.add("go-up");
    }
  });
  aboutLefts.forEach((aboutLeft) => {
    if (isInViewport(aboutLeft)) {
      aboutLeft.classList.add("go-right");
    }
  });
  aboutRights.forEach((aboutRight) => {
    if (isInViewport(aboutRight)) {
      aboutRight.classList.add("go-left");
    }
  });
  if (
    isInViewport(statsData) &&
    !statsData.classList.contains("increased-number")
  ) {
    statsData.classList.add("increased-number");
    numberIncreasingRecursion(0, 100, satisfaction, 10);
    numberIncreasingRecursion(0, 24, hoursSupport, 30);
    numberIncreasingRecursion(0, 7, daysSupport, 30);
    numberIncreasingRecursion(0, 100, sales, 10);
  }
  if (isInViewport(pricingColumns[0])) {
    pricingColumns[0].classList.add("go-up");
  }
  if (
    isInViewport(pricingColumns[1]) &&
    !pricingColumns[1].classList.contains("go-up")
  ) {
    pricingColumns[1].style.opacity = "0";
    setTimeout(() => {
      pricingColumns[1].style.opacity = "1";
      pricingColumns[1].classList.add("go-up");
    }, 250);
  }
});
